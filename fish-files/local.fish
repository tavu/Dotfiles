# default env vars
set -x PATH $PATH ~/go/bin ~/bin ~/.local/bin
set -x PATH $PATH:$HOME/bin:$HOME/.gcloud/google-cloud-sdk/bin
set -x GOOGLE_CLOUD_KEYFILE_JSON $HOME/.gcloud/sa-workshop.json
set -x EDITOR vim

# enable fzf key bindings
fzf_key_bindings
